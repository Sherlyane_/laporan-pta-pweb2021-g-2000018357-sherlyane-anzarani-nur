<!DOCTYPE html>
<html>
<head>
	<title>Menghitung BMI</title>
	<style type="text/css">
        body{
        	background-image: url(bg12.jpg);
	        background-size: cover; 
        }
		#box{
			height: 450px;
			width: 450px;
			text-align: center;
			background-color: rosybrown;
			margin: 60px auto;
			border-radius: 30px;
			padding: 10px;
		}
		#berat{
			width: 350px;
			height: 50px;
			margin: 15px;
		}
		#tinggi{
			width: 350px;
			height: 50px;
			margin: 15px;
		}
		button {
	        width: 120px;
	        height: 40px;
	        background-color: white;
	        padding: 5px;
	        border-radius: 30px;
	        margin-top: 25px;
	        font-size: 15px;
	        color: rosybrown;
        }
        button:hover{
	        transform: scale(1.2);
        }
		p{
			text-align: center;
			font-size: 40px;
			margin: 20px;
		}
	</style>
</head>
<body>
<div id="box">
<form action="hasilBMI.php" method="post" name="input">
<p> Program Menghitung BMI </p>
<input type="text" name="berat" id="berat" placeholder="Masukan Berat Badan"><br>
<input type="text" name="tinggi" id="tinggi" placeholder="Masukan Tinggi Badan"><br>
<button type="submit">Submit</button>
</form>
</div>

</body>

</html>

